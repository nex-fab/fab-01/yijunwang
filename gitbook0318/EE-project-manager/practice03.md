# fab-lab 第三次课程
## 1.使用的工具 [Ultimaker Cura](https://ultimaker.com/software/ultimaker-cura)

![](https://gitlab.com/testpic/picbed/uploads/5db9983f5a8516e33f2610360432dc7f/1.jpg)

这是一个免费的3D切片软件，可以直接在官网下载正版。

## 2.新建1个打印机
我们在第一次打开Cura软件时，需要按照按照自己的设备情况建立打印机的打印空间。
我们需要建立一个不是连接在网络上的打印机，自定义1个FFF打印机。  

![](https://gitlab.com/testpic/picbed/uploads/54c3bff03caf232f56935042f2ad4fc3/2.jpg)  

打印机的名字并不重要，主要是设置的参数要对，打印的空间大小决定了可以打印多大的器件。

下图中，需要将红框位置内XYZ三轴的尺寸设置为需要的大小，设置完成后，箭头所指的立方体框会变为你设置的大小。

![](https://gitlab.com/testpic/picbed/uploads/23d28e8de7d06eadfd736ebd885f03ac/3.jpg)

第二页中喷嘴的尺寸及位置也是比较重要的，虽然这次是使用的默认数值，但是在之后新建打印机属性时需要注意匹配。

![](https://gitlab.com/testpic/picbed/uploads/62348ceaefc29d0ff2b311fae4a957cb/4.jpg)

## 3.打印机使用的文件生成
目前接触到的Cura使用的文件扩展名为*.stl。我们之前使用的Fusion360可以将零部件保存为这个格式，再通过Cura打开。
或者直接通过Fusion360的接口，将文件同步到Cura中。

![](https://gitlab.com/testpic/picbed/uploads/68b017fa10807744700a8da812ddb522/5.jpg)

在Cure软件中，需要设置打印时的参数。这里主要是依据打印机属性和经验，常规情况按照目前的参数就可以了。

![](https://gitlab.com/testpic/picbed/uploads/c63a343fb8e861d5e46b9a086e90473e/6.jpg)

参数设置完成后，可以查看到完成打印所需的时间。此时点击窗口上方的PREVIEW，可以查看模拟打印的情况。
如下图，右侧的挑可以调整模拟打印的层次位置，下方的是当前层打印的进度条。点击开始后会开始打印，直至最高1层。

![](https://gitlab.com/testpic/picbed/uploads/a478b820e1f04bad6b631c003f097ca6/7.jpg)

![](https://gitlab.com/testpic/picbed/uploads/7f95eaeeb50cacc8ed1a71b829b5422e/8.gif)

生成Gcode文件后就可以保存到SD卡中，到打印机上打印了。

## 4.实际打印
首先给大家看一下第一次打印的情况。

![](https://gitlab.com/testpic/picbed/uploads/b57473202c9b2a46304e2d611c2a8233/10.gif)

下面是打印到一半的实物。

![](https://gitlab.com/testpic/picbed/uploads/37a236a9fa7e3c6e01b522511a65b9ab/9.jpg)

可以看到材料之间非常的疏松，并不是实心体，所以打印到一半就停止了，这是打印机存在一些问题。

打印机跟换材料后，有可能会在喷嘴处存在残留的污物，需要将残留在内壁的污物取出干净

![](https://gitlab.com/testpic/picbed/uploads/4d5d95603a97d97c252cc8adc6745468/11.jpg)

以下是打印机喷头拆解过程：

![](https://gitlab.com/testpic/picbed/uploads/bbcbeca538e35a52978563b6df0b6679/14.jpg)

![](https://gitlab.com/testpic/picbed/uploads/51ad377f43594c10eab6cd53a826987d/18.jpg)

![](https://gitlab.com/testpic/picbed/uploads/9a3ebd42f9b5cac404e3af547edceba9/12.jpg)

![](https://gitlab.com/testpic/picbed/uploads/e339a7cef17b592200e437bc1dff8a12/17.jpg)

拆下周围零件后，需要将喷嘴温度升至200℃，并手动向喷嘴中送料，观察喷嘴下出来的材料的情况，待感觉正常后再装回。

下图是第二次打印完成的情况，虽然还是很惨但相对而言还是好了不少。

![](https://gitlab.com/testpic/picbed/uploads/9f4e57356d7c24f3202572eae1f5065b/20.jpg)