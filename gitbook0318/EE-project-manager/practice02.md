# fab-lab 第二次课程
## 1.使用的工具-Fusion 360
在本次课程中，我们只需要用到一个软件，即Fusion 360  
![](https://gitlab.com/testpic/picbed/uploads/d6f053e6bcf0cd5096b673f72498a61d/1.png)  

这是一个三维设计CAD软件，体积比较小，文件的处理及存储都是在云端，比较适合电脑配置不是特别好的用户。
登录账户后，可以在软件中选择建立或者加入别人的项目组，我加入了Nexpcb-Fab01的项目组，在项目组中建立了自己的文件夹，就可以将自己设计的零件保存在项目组中，处于同一项目组的同事也能看见并帮助修改或者完善设计，比较适合多个人合作完成一个项目。  

![](https://gitlab.com/testpic/picbed/uploads/2835956647f76ac76d19f419e2d7721e/2.png)  

## 2.新建设计
我们现在刚接触结构设计的软件，初次尝试是从图纸新建一个设计。  
新建设计比较简单，直接点"+"就可以新建一个设计文件了。在设计中，我觉得草图的平面选择是非常重要的。当一个设计有多个多个零部件时，选择一个固定的平面作为参考系能够比较方便且直观的看出部件之间的搭配组装。  
![](https://gitlab.com/testpic/picbed/uploads/784b4bd4a43a1cdcd7a5d67d4a7069e2/3.png)
接下来我们主要是尝试了一些常规的操作，如在平面的图纸上画圈画矩形，并尝试使用了一些工具使平面设计变为立体的部件，如拉伸等。  

![](https://gitlab.com/testpic/picbed/uploads/537fa618cd1548bdb675a4861565140e/4.png)  

之后我们尝试制作了2个零部件，并将2个零部件通过装配功能连接到一起。  
![](https://gitlab.com/testpic/picbed/uploads/224ae4f52a8b2c043698e762d49d1fde/5.png)  

在进行装配时需要将其中一个零部件作为基准，设为固定。并且在可活动的装配中，设置接触集合是非常重要的，不然零部件之间没有碰撞体积，也就是会穿模。  
![](https://gitlab.com/testpic/picbed/uploads/b2ad151661735ed9f5c9938bd6c449f5/6.png)  

因为软件存在一些问题，所以只要你<font face="宋体" color=red size=156>手速够快！！！</font>也是可以无视碰撞体积的。

## 3.生成图纸
在完成一个零部件或者整个成品后，我们可以选择生成2D的图纸，工厂需要用到2D的图纸才能够生产。  
![](https://gitlab.com/testpic/picbed/uploads/f7c6bd2a2fb8af366845a40f0a38f3c1/7.png)  

图纸一般选择横向A4的纸，这样正好可以打印出来，具体的比例看零部件的大小。放置零件时需要选择一个主视角，所以之前在草图中进行平面选择时比较重要，不然看起来会比较奇怪。  
![](https://gitlab.com/testpic/picbed/uploads/443b8e73c46ce7596f0d6e037d7a20d3/8.png)  

图纸上面的尺寸标注是非常重要的，在进行标注时最好按照一个固定的顺序，不然容易漏掉。

## 4.自己的设计
我原本是想要设计一个保险箱，但是在过程中发现高估了自己的实力，然后箱子的锁就没有了。现在箱子就2个部件，虽然看起来还是像个箱子，但是实际上如果材料不是软的，那这个箱子应该装不起来。

<iframe src="https://myhub.autodesk360.com/ue28cacf9/shares/public/SH919a0QTf3c32634dcf1fb0710db3816940?mode=embed" width="1024" height="768" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>  

设计的过程中发现启用接触集合后轴的那一端转不进去，然后就直接掏了一个半圆出来，虽然比较丑，但是功能实现了呀。  
![](https://gitlab.com/testpic/picbed/uploads/9cac784435b064f1ba52d06f36171922/9.png)  

然后另一边就做了个角度，防止盖子进不去。总体来说还是一个比较简单的设计，还有一些能够改进的地方。
在修改的过程中，发现下面的进度条还是比较好用的，能够直接选择跳到对应的步骤进行修改，如果有问题还能直接删除一个步骤。

![](https://gitlab.com/testpic/picbed/uploads/239b4d3921d92bd9f391f7eb4b5a48a2/10.png)