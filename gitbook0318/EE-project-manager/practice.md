# fab-lab第一次课程
## 1.软件安装
在本次课程的学习中，需要用到Git,picgo,vs code。安装过程基本没太大的问题，插件的安装需要选择正确，网络不好使的话需要多尝试下载几次。
## 2.[开发者网站](https://gitlab.com)
我们的fab-lab课程选择的是gitlab，与github有有一些相似的地方，之前工作中有些客户会把文件放到github上。接触开发者网站对我们的工作也有些帮助。
![](https://gitlab.com/testpic/picbed/uploads/e9757f737cecb9c2741e749a02ef55f9/1.png)
老师准备的[说明书](https://www.nexmaker.com/)还是比较好使的，大部分问题都能在上面找到解答。
![](https://gitlab.com/testpic/picbed/uploads/748793b8565441210e71facbf5996523/2.png)
## 3.使用gitlab
我们将文件存储在建立于gitlab的项目中，同时也在本地建立一个放置工作文件的区域。我们需要将2个地方的文件保持为同一个版本，这就需要我们建立一个云端文件和本地文件同步传输的通道，再通过git-bash这个工具完成文件从云端到本地，或者本地到云端的同步操作。  

![](https://gitlab.com/testpic/picbed/uploads/78d4cab9eed6bee0e667312a80fe50f8/3.png)  

通过git-bash新建文件夹并与gitlab个人项目组进行连接的过程比较顺利，就不多描述了，说明书上面详细说明了整个过程。
git clone是一个将云端库复制一个备份到本地的过程，主要是用于一个新的终端的建立。
git pull则是将本地文件和云端文件合并更新的过程，后续的文件变更更新主要也是用这个指令。
git push是将本地文件合并更新到gitlab网站上，比较幸运的是我在运行这个指令的时候一次就成功了，没有遇到暂时无法解答的问题。
## 4.使用vscode进行文件编辑
本次课程接触到了新的语言markdown。Markdown是一种可以使用普通文本编辑器编写的标记语言，通过简单的标记语法，它可以使普通文本内容具有一定的格式。
vscode是一款可以使用多种语言的IDE软件，因此，在进行文件编辑时，请一定要注意将右下角选为markdown。
![](https://gitlab.com/testpic/picbed/uploads/76e52995c764a1a86581cd4533e66e35/4.png)  

此外，vscode有比较强大的插件功能，在Extensions中有不少比较好用的插件，即时显示效果的插件是目前唯一使用的功能，看起来还是比较好用的。

![](https://gitlab.com/testpic/picbed/uploads/e91b1f6cb424adb8ebedd3d65a86fce5/5.png)

实时的效果可以看下图
![](https://gitlab.com/testpic/picbed/uploads/e47bc6c4377a4547861581ffd13c2d55/6.png)
<font face="宋体" color=red size=6>然后是一些比较常见的如改变字体颜色什么的</font>

下面是一条分割线
******
虽然并没有什么用。

## 5.图床工具
这个工具目前使用的不深入，但是根据文件界面来看，应该功能还是蛮强大的，能够将文件保存在云端并生成链接，还能根据选择的语言自动生成语言规范的链接。
![](https://gitlab.com/testpic/picbed/uploads/07ba3ab494c450853704b1d28de23e0e/7.png)  

在使用这个工具时，进行图床设置的时候不太顺利，用的token好像和gitlab端建立的项目属性有关系，可能当时在建的时候没有选择完全公开。需要重新建一个。
![](https://gitlab.com/testpic/picbed/uploads/df82b746e711f7db88f018f6c5960da3/8.png)  

然后可以通过这git push指令将本地编写好的文件同步到gitlab上，让别人能看见编写的网页。  

![](https://gitlab.com/testpic/picbed/uploads/d82c8d73ecf93165711a042bd68a0764/9.png)  

大致是这些工具的入门和简单运用，详细的细节就不特别说明了。大概就这些。